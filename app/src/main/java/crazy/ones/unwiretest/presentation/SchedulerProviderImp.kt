package crazy.ones.unwiretest.presentation

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class SchedulerProviderImp: SchedulerProvider {
    override fun io() = Schedulers.io()

    override fun mainThread() = AndroidSchedulers.mainThread()
}