package crazy.ones.unwiretest.presentation.feature.shirts

import android.os.Bundle
import android.view.Menu
import android.view.MenuInflater
import android.view.MenuItem
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import crazy.ones.unwiretest.R
import crazy.ones.unwiretest.business.entity.FilterItems
import crazy.ones.unwiretest.business.entity.Shirt
import crazy.ones.unwiretest.presentation.BaseActivity
import crazy.ones.unwiretest.presentation.StatefulData
import crazy.ones.unwiretest.presentation.feature.ShirtsAdapter
import crazy.ones.unwiretest.presentation.feature.shirtDetails.ShirtDetailsActivity
import crazy.ones.unwiretest.presentation.feature.shoppingcard.BasketActivity
import kotlinx.android.synthetic.main.activity_shirts.*
import kotlinx.android.synthetic.main.content_toolbar.*
import javax.inject.Inject

class ShirtsActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: ShirtsViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ShirtsViewModel::class.java)
    }

    private val snackBar: Snackbar by lazy {
        Snackbar.make(constraint_layout_shirts, "", Snackbar.LENGTH_LONG)
    }

    private val shirtsAdapter: ShirtsAdapter by lazy {
        ShirtsAdapter(this, onAddOrRemoveClickListener = ::onAddOrRemoveClick, onClickListener = ::onListItemClick)
    }

    private fun onAddOrRemoveClick(shirt: Shirt) {
        viewModel.addOrRemoveFromBasket(shirt)
    }

    private fun onListItemClick(shirt: Shirt) {
        startActivity(ShirtDetailsActivity.create(this, shirt.id))
    }

    private var filterDialog: ShirtsFilterDialog? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shirts)

        initViews()

        viewModel.getShirts().observe(this, Observer { shirts ->
            shirtsAdapter.submitList(shirts)
        })

        viewModel.getNetworkState().observe(this, Observer { networkState ->
            when (networkState) {
                is StatefulData.Loading -> {
                    swipe_refresh_shirts.isRefreshing = networkState.isLoading
                }
                is StatefulData.Failure -> {
                    swipe_refresh_shirts.isRefreshing = false
                    snackBar.setText(networkState.message)
                    snackBar.show()
                }
                else -> {
                    // it doesn't happen
                }
            }
        })

        viewModel.getFilterItems().observe(this, Observer { items ->
            filterDialog = ShirtsFilterDialog(this, items, object : ShirtsFilterDialog.OnApplyFilterListener {
                override fun onApply(filterItems: FilterItems) {
                    filterItems.takeUnless { it.colors.isNullOrEmpty() && it.sizes.isNullOrEmpty() }?.apply {
                        viewModel.getByFilter(filterItems)
                    } ?: run {
                        viewModel.getData()
                    }
                    filterDialog?.dismiss()
                }
            })
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        val inflater: MenuInflater = menuInflater
        inflater.inflate(R.menu.menu_shirts_list, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.filter -> {
                filterDialog?.show() ?: run {
                    Toast.makeText(
                        this,
                        getString(R.string.no_item_filter),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initViews() {
        toolbar.title = getString(R.string.shirts)
        setSupportActionBar(toolbar)

        swipe_refresh_shirts.setOnRefreshListener {
            viewModel.refresh()
        }

        recycler_view_shirts.apply {
            layoutManager = LinearLayoutManager(this@ShirtsActivity, RecyclerView.VERTICAL, false)
            adapter = shirtsAdapter
        }

        fab_shirts_basket.setOnClickListener {
            startActivity(BasketActivity.create(this))
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        filterDialog?.dismiss()
        snackBar.dismiss()
    }
}