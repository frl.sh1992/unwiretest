package crazy.ones.unwiretest.presentation.util

import android.content.Context
import android.view.View
import androidx.annotation.DrawableRes
import androidx.appcompat.content.res.AppCompatResources

fun View.visible() {
    visibility = View.VISIBLE
}

fun View.invisible() {
    visibility = View.INVISIBLE
}

fun View.gone() {
    visibility = View.GONE
}

fun View.visibleBy(visible: Boolean) {
    visibility = when (visible) {
        true -> View.VISIBLE
        false -> View.GONE
    }
}

fun View.setEnable(enable: Boolean) {
    isEnabled = enable
    isClickable = enable
    isFocusable = enable
}

fun Context.getCompatDrawable(@DrawableRes id: Int) = AppCompatResources.getDrawable(this, id)