package crazy.ones.unwiretest.presentation.feature

import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import crazy.ones.unwiretest.R
import crazy.ones.unwiretest.business.entity.Shirt
import crazy.ones.unwiretest.presentation.util.getCompatDrawable
import kotlinx.android.synthetic.main.item_shirts_list.view.*

class ShirtsAdapter(
    private val context: Context,
    private val onAddOrRemoveClickListener: (Shirt) -> Unit,
    private val onClickListener: ((Shirt) -> Unit)? = null
) : ListAdapter<Shirt, ShirtsAdapter.ShirtsViewHolder>(DiffCallback) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ShirtsViewHolder {
        val itemView: View = LayoutInflater.from(context).inflate(R.layout.item_shirts_list, parent, false)
        return ShirtsViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: ShirtsViewHolder, position: Int) {
        holder.bind(getItem(position), onClickListener, { shirt ->
            onAddOrRemoveClickListener(shirt)
        })
    }

    class ShirtsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(shirt: Shirt, onClickListener: ((Shirt) -> Unit)? = null, onAddClickListener: (Shirt) -> Unit) {
            itemView.apply {
                Glide.with(context).load(shirt.pictureUrl).placeholder(R.mipmap.ic_launcher)
                    .into(image_view_shirts_item)
                text_view_shirts_item_name.text = shirt.name
                text_view_shirts_item_color.text = context.getString(R.string.color, shirt.colour)
                text_view_shirts_item_size.text = context.getString(R.string.size, shirt.size)
                onClickListener?.let { onClicked ->
                    constraint_layout_container.setOnClickListener {
                        onClicked(shirt)
                    }
                } ?: run {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        constraint_layout_container.foreground = null
                    }
                }
                image_view_shirts_items_add.apply {
                    setImageDrawable(
                        context.getCompatDrawable(
                            when (shirt.isInBasket) {
                                true -> R.drawable.ic_remove_circle
                                false -> R.drawable.ic_add_circle
                            }
                        )
                    )
                    setOnClickListener {
                        onAddClickListener(shirt)
                    }
                }
            }
        }
    }

    object DiffCallback : DiffUtil.ItemCallback<Shirt>() {
        override fun areItemsTheSame(oldItem: Shirt, newItem: Shirt): Boolean =
            oldItem.id == newItem.id

        override fun areContentsTheSame(oldItem: Shirt, newItem: Shirt): Boolean =
            oldItem == newItem
    }
}