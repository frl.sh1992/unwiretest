package crazy.ones.unwiretest.presentation

sealed class StatefulData<T> {

    data class Success<T>(val data: T): StatefulData<T>()
    data class Failure<T>(val message: String): StatefulData<T>()
    data class Loading<T>(val isLoading: Boolean = true) : StatefulData<T>()
}