package crazy.ones.unwiretest.presentation.feature.shoppingcard

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.snackbar.Snackbar
import crazy.ones.unwiretest.R
import crazy.ones.unwiretest.business.entity.Shirt
import crazy.ones.unwiretest.presentation.BaseActivity
import crazy.ones.unwiretest.presentation.StatefulData
import crazy.ones.unwiretest.presentation.feature.ShirtsAdapter
import crazy.ones.unwiretest.presentation.util.gone
import crazy.ones.unwiretest.presentation.util.setEnable
import crazy.ones.unwiretest.presentation.util.visible
import kotlinx.android.synthetic.main.activity_basket.*
import javax.inject.Inject

class BasketActivity : BaseActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: BasketViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(BasketViewModel::class.java)
    }

    private val snackBar: Snackbar by lazy {
        Snackbar.make(recycler_view_basket, "", Snackbar.LENGTH_LONG)
    }
    private val shirtsAdapter: ShirtsAdapter by lazy {
        ShirtsAdapter(this, onAddOrRemoveClickListener = {
            viewModel.removeFromBasket(it)
        })
    }
    private var shirts: List<Shirt> = listOf()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_basket)

        initViews()

        viewModel.getShirts().observe(this, Observer { dataState ->
            when (dataState) {
                is StatefulData.Success -> {
                    dataState.data.takeUnless { it.isNullOrEmpty() }?.let { shirts ->
                        text_view_price.text = getString(R.string.total_price, shirts.sumBy { it.price.toInt() })
                        text_view_empty.gone()
                        text_view_basket_order.setEnable(true)
                    } ?: run {
                        text_view_price.text = ""
                        text_view_empty.visible()
                        text_view_basket_order.setEnable(false)
                    }
                    shirts = dataState.data
                    shirtsAdapter.submitList(dataState.data)
                    snackBar.dismiss()
                }
                is StatefulData.Failure -> {
                    snackBar.setText(dataState.message)
                    snackBar.show()
                }
                else -> {
                    // do nothing
                }
            }
        })

        viewModel.getOrder().observe(this, Observer { dataState ->
            when (dataState) {
                is StatefulData.Loading -> {
                    progress_bar_basket_order.visible()
                }
                is StatefulData.Success -> {
                    progress_bar_basket_order.gone()
                    Toast.makeText(
                        applicationContext,
                        getString(R.string.order_status_is, dataState.data.status),
                        Toast.LENGTH_SHORT
                    ).show()
                }
                is StatefulData.Failure -> {
                    progress_bar_basket_order.gone()
                    snackBar.setText(dataState.message)
                    snackBar.show()
                }
            }
        })
    }

    private fun initViews() {
        setDisplayHomeEnabled()

        recycler_view_basket.apply {
            layoutManager = LinearLayoutManager(this@BasketActivity, RecyclerView.VERTICAL, false)
            adapter = shirtsAdapter
        }
        text_view_basket_order.setOnClickListener {
            viewModel.submitOrder(shirts)
        }
    }

    companion object {
        fun create(context: Context): Intent = Intent(context, BasketActivity::class.java)
    }
}