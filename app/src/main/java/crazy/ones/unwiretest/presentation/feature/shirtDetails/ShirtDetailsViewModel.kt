package crazy.ones.unwiretest.presentation.feature.shirtDetails

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import crazy.ones.unwiretest.business.entity.Shirt
import crazy.ones.unwiretest.business.repository.shirt.ShirtRepository
import crazy.ones.unwiretest.presentation.BaseViewModel
import crazy.ones.unwiretest.presentation.SchedulerProvider
import javax.inject.Inject

class ShirtDetailsViewModel @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val shirtRepository: ShirtRepository
) : BaseViewModel(schedulerProvider) {

    private val shirtLiveData = MutableLiveData<Shirt>()

    fun getShirt(): LiveData<Shirt> = shirtLiveData

    fun loadShirt(id: Long) {
        shirtRepository.get(id).prepare().subscribe({
            shirtLiveData.value = it
        }, {
            it.printStackTrace()
        }).also { compositeDisposable.add(it) }
    }
}