package crazy.ones.unwiretest.presentation

import androidx.lifecycle.ViewModel
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable

abstract class BaseViewModel(
    private val schedulerProvider: SchedulerProvider
) : ViewModel() {

    protected val compositeDisposable = CompositeDisposable()

    fun <T> Single<T>.prepare(): Single<T> =
        this.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.mainThread())

    fun <T> Flowable<T>.prepare(): Flowable<T> =
        this.subscribeOn(schedulerProvider.io()).observeOn(schedulerProvider.mainThread())

    override fun onCleared() {
        super.onCleared()
        compositeDisposable.clear()
    }
}