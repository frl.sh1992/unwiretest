package crazy.ones.unwiretest.presentation.feature.shoppingcard

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import crazy.ones.unwiretest.business.entity.Order
import crazy.ones.unwiretest.business.entity.Shirt
import crazy.ones.unwiretest.business.network.ErrorParser
import crazy.ones.unwiretest.business.repository.order.OrderRepository
import crazy.ones.unwiretest.business.repository.shirt.ShirtRepository
import crazy.ones.unwiretest.presentation.BaseViewModel
import crazy.ones.unwiretest.presentation.SchedulerProvider
import crazy.ones.unwiretest.presentation.StatefulData
import javax.inject.Inject

class BasketViewModel @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val shirtRepository: ShirtRepository,
    private val orderRepository: OrderRepository,
    private val errorParser: ErrorParser
) : BaseViewModel(schedulerProvider) {

    private val basketLiveData: MutableLiveData<StatefulData<List<Shirt>>> = MutableLiveData()
    private val orderLiveData: MutableLiveData<StatefulData<Order>> = MutableLiveData()

    init {
        getInBasketShirts()
    }

    fun getShirts(): LiveData<StatefulData<List<Shirt>>> = basketLiveData
    fun getOrder(): LiveData<StatefulData<Order>> = orderLiveData

    fun removeFromBasket(shirt: Shirt) {
        shirtRepository.toggleShirtFromBasket(shirt)
    }

    fun submitOrder(shirts: List<Shirt>) {
        orderRepository.submitOrder(shirts).prepare().doOnSubscribe {
            orderLiveData.value = StatefulData.Loading()
        }.subscribe({ order ->
            orderLiveData.value = StatefulData.Success(order)
            shirtRepository.clearBasket()
        }, {
            orderLiveData.value = StatefulData.Failure(errorParser.parse(it))
        }).also { compositeDisposable.add(it) }
    }

    private fun getInBasketShirts() =
        shirtRepository.getInBasketShirts().prepare().subscribe({
            basketLiveData.value = StatefulData.Success(it)
        }, {
            basketLiveData.value = StatefulData.Failure(errorParser.parse(it))
        }).also { compositeDisposable.add(it) }
}