package crazy.ones.unwiretest.presentation

import android.graphics.PorterDuff
import android.view.MenuItem
import androidx.core.content.ContextCompat
import dagger.android.support.DaggerAppCompatActivity
import kotlinx.android.synthetic.main.content_toolbar.*

open class BaseActivity : DaggerAppCompatActivity() {

    fun setDisplayHomeEnabled() {
        setSupportActionBar(toolbar)
        supportActionBar?.apply {
            setDisplayHomeAsUpEnabled(true)
            setDisplayShowHomeEnabled(true)
        }
        toolbar.navigationIcon?.setColorFilter(
            ContextCompat.getColor(this, android.R.color.white),
            PorterDuff.Mode.SRC_ATOP
        )
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}