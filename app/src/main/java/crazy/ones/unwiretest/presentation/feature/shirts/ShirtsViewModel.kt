package crazy.ones.unwiretest.presentation.feature.shirts

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import crazy.ones.unwiretest.business.entity.FilterItems
import crazy.ones.unwiretest.business.entity.Shirt
import crazy.ones.unwiretest.business.network.ErrorParser
import crazy.ones.unwiretest.business.repository.shirt.ShirtRepository
import crazy.ones.unwiretest.presentation.BaseViewModel
import crazy.ones.unwiretest.presentation.SchedulerProvider
import crazy.ones.unwiretest.presentation.StatefulData
import io.reactivex.disposables.Disposable
import javax.inject.Inject

class ShirtsViewModel @Inject constructor(
    schedulerProvider: SchedulerProvider,
    private val shirtRepository: ShirtRepository,
    private val errorParser: ErrorParser
) : BaseViewModel(schedulerProvider) {

    private val shirtsLivaData: MutableLiveData<List<Shirt>> = MutableLiveData()
    private val networkLiveData: MutableLiveData<StatefulData<Nothing>> = MutableLiveData()
    private val filterItemsLiveData: MutableLiveData<FilterItems> = MutableLiveData()

    private var lastShirtsDisposable: Disposable? = null

    init {
        getData()
        refresh()
    }

    fun getShirts(): LiveData<List<Shirt>> = shirtsLivaData

    fun getNetworkState(): LiveData<StatefulData<Nothing>> = networkLiveData

    fun getFilterItems(): LiveData<FilterItems> = filterItemsLiveData

    fun getData() {
        shirtRepository.getAll().prepare().doOnSubscribe {
            lastShirtsDisposable?.dispose()
        }.subscribe({
            shirtsLivaData.value = it
            setFilterItems(it)
        }, {
            // it's almost impossible
            it.printStackTrace()
        }).also {
            lastShirtsDisposable = it
            compositeDisposable.add(it)
        }
    }

    fun refresh() {
        shirtRepository.refresh().prepare().doOnSubscribe {
            networkLiveData.value = StatefulData.Loading(true)
        }.subscribe({
            networkLiveData.value = StatefulData.Loading(false)
        }, {
            it.printStackTrace()
            networkLiveData.value = StatefulData.Failure(errorParser.parse(it))
        }).also { compositeDisposable.add(it) }
    }

    fun getByFilter(filterItems: FilterItems) {
        shirtRepository.get(filterItems).prepare().doOnSubscribe {
            lastShirtsDisposable?.dispose()
        }.subscribe({
            shirtsLivaData.value = it
        }, {
            // it's almost impossible
            it.printStackTrace()
        }).also {
            lastShirtsDisposable = it
            compositeDisposable.add(it)
        }
    }

    fun addOrRemoveFromBasket(shirt: Shirt) =
        shirtRepository.toggleShirtFromBasket(shirt)

    private fun setFilterItems(shirts: List<Shirt>) {
        val filterItems = FilterItems(mutableSetOf(), mutableSetOf())
        shirts.forEach {
            filterItems.colors.add(it.colour)
            filterItems.sizes.add(it.size)
        }
        filterItemsLiveData.value = filterItems
    }

    override fun onCleared() {
        super.onCleared()
        lastShirtsDisposable?.dispose()
    }
}