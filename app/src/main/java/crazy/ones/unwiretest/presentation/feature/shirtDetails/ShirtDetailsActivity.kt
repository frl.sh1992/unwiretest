package crazy.ones.unwiretest.presentation.feature.shirtDetails

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.ViewModelProviders
import com.bumptech.glide.Glide
import crazy.ones.unwiretest.R
import crazy.ones.unwiretest.presentation.BaseActivity
import kotlinx.android.synthetic.main.activity_shirt_details.*
import kotlinx.android.synthetic.main.content_toolbar.*
import javax.inject.Inject

class ShirtDetailsActivity : BaseActivity() {

    private val shirtId: Long by lazy {
        intent.extras?.getLong(EXTRAS_SHIRT) ?: throw IllegalArgumentException("shirt id shouldn't be null")
    }

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    private val viewModel: ShirtDetailsViewModel by lazy {
        ViewModelProviders.of(this, viewModelFactory).get(ShirtDetailsViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_shirt_details)

        initViews()
        viewModel.loadShirt(shirtId)
    }

    private fun initViews() {
        toolbar.title = getString(R.string.shirt_details)
        setDisplayHomeEnabled()

        viewModel.getShirt().observe(this, Observer {
            Glide.with(this@ShirtDetailsActivity)
                .load(it.pictureUrl)
                .placeholder(R.mipmap.ic_launcher)
                .into(image_view_shirt_details)
            text_view_shirt_details_name.text = it.name
            text_view_shirt_details_color.text = getString(R.string.color, it.colour)
            text_view_shirt_details_price.text = getString(R.string.price, it.price)
            text_view_shirt_details_size.text = getString(R.string.size, it.size)
            text_view_shirt_details_quantifier.text = getString(R.string.quantity, it.quantity)
        })
    }

    companion object {

        private const val EXTRAS_SHIRT = "EXTRAS_SHIRT"

        fun create(context: Context, shirtId: Long): Intent {
            val intent = Intent(context, ShirtDetailsActivity::class.java)
            intent.putExtra(EXTRAS_SHIRT, shirtId)
            return intent
        }
    }
}