package crazy.ones.unwiretest.presentation.feature.shirts

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.view.Window
import android.widget.CheckBox
import crazy.ones.unwiretest.R
import crazy.ones.unwiretest.business.entity.FilterItems
import kotlinx.android.synthetic.main.dialog_shirts_filter.*

class ShirtsFilterDialog(
    context: Context,
    private val items: FilterItems,
    private val onApplyFilterListener: OnApplyFilterListener
) : Dialog(context, R.style.DialogTheme) {

    private val selectedItems: FilterItems = FilterItems(mutableSetOf(), mutableSetOf())

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requestWindowFeature(Window.FEATURE_NO_TITLE)
        setContentView(R.layout.dialog_shirts_filter)

        items.apply {
            sizes.forEach { size ->
                val checkBox = CheckBox(context)
                checkBox.text = size
                checkBox.setOnClickListener {
                    when (checkBox.isChecked) {
                        true -> selectedItems.sizes.add(size)
                        false -> selectedItems.sizes.remove(size)
                    }
                }
                linear_layout_filter_dialog_sizes.addView(checkBox)
            }

            colors.forEach { color ->
                val checkBox = CheckBox(context)
                checkBox.text = color
                checkBox.setOnClickListener {
                    when (checkBox.isChecked) {
                        true -> selectedItems.colors.add(color)
                        false -> selectedItems.colors.remove(color)
                    }
                }
                linear_layout_filter_dialog_colors.addView(checkBox)
            }
        }

        button_filter_dialog_submit.setOnClickListener {
            onApplyFilterListener.onApply(selectedItems)
        }
    }

    interface OnApplyFilterListener {
        fun onApply(filterItems: FilterItems)
    }
}