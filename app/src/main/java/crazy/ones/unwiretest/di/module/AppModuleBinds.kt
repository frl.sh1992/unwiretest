package crazy.ones.unwiretest.di.module

import android.app.Application
import crazy.ones.unwiretest.presentation.MainApplication
import dagger.Binds
import dagger.Module

@Module
interface AppModuleBinds {

    @Binds
    fun providesApplication(application: MainApplication): Application
}