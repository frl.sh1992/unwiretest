package crazy.ones.unwiretest.di.module

import android.content.Context
import androidx.room.Room
import crazy.ones.unwiretest.business.network.ErrorParser
import crazy.ones.unwiretest.business.network.ErrorParserImp
import crazy.ones.unwiretest.business.network.OrderApi
import crazy.ones.unwiretest.business.network.ShirtApi
import crazy.ones.unwiretest.business.persist.AppDatabase
import crazy.ones.unwiretest.business.persist.ShirtDao
import crazy.ones.unwiretest.business.repository.order.OrderRepository
import crazy.ones.unwiretest.business.repository.order.OrderRepositoryImp
import crazy.ones.unwiretest.business.repository.shirt.ShirtRepository
import crazy.ones.unwiretest.business.repository.shirt.ShirtRepositoryImp
import crazy.ones.unwiretest.presentation.MainApplication
import crazy.ones.unwiretest.presentation.SchedulerProvider
import crazy.ones.unwiretest.presentation.SchedulerProviderImp
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Singleton

@Module(includes = [AppModuleBinds::class])
class AppModule {

    @Provides
    fun provideContext(application: MainApplication): Context = application.applicationContext

    @Provides
    @Singleton
    fun provideScheduler(): SchedulerProvider = SchedulerProviderImp()

    @Provides
    @Singleton
    fun provideShirtRepository(shirtApi: ShirtApi, shirtDao: ShirtDao): ShirtRepository =
        ShirtRepositoryImp(shirtApi, shirtDao)

    @Provides
    @Singleton
    fun provideShirtApi(retrofit: Retrofit): ShirtApi =
        retrofit.create(ShirtApi::class.java)

    @Provides
    @Singleton
    fun provideShirtDao(database: AppDatabase): ShirtDao = database.getShirtDao()

    @Provides
    @Singleton
    fun provideDatabase(context: Context): AppDatabase =
        Room.databaseBuilder(context.applicationContext, AppDatabase::class.java, "database")
            .allowMainThreadQueries().build()

    @Provides
    @Singleton
    fun provideErrorParser(context: Context): ErrorParser = ErrorParserImp(context)

    @Provides
    @Singleton
    fun provideOrderRepository(orderApi: OrderApi): OrderRepository = OrderRepositoryImp(orderApi)

    @Provides
    @Singleton
    fun provideOrderApi(retrofit: Retrofit): OrderApi = retrofit.create(OrderApi::class.java)
}