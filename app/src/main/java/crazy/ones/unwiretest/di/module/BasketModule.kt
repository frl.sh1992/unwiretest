package crazy.ones.unwiretest.di.module

import androidx.lifecycle.ViewModel
import crazy.ones.unwiretest.di.ViewModelKey
import crazy.ones.unwiretest.presentation.feature.shoppingcard.BasketActivity
import crazy.ones.unwiretest.presentation.feature.shoppingcard.BasketViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
interface BasketModule {

    @ContributesAndroidInjector
    fun basketActivity(): BasketActivity

    @Binds
    @IntoMap
    @ViewModelKey(BasketViewModel::class)
    fun binBasketViewModel(viewModel: BasketViewModel): ViewModel
}