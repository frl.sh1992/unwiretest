package crazy.ones.unwiretest.di.module

import androidx.lifecycle.ViewModelProvider
import crazy.ones.unwiretest.di.BaseViewModelFactory
import dagger.Binds
import dagger.Module

@Module
internal abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: BaseViewModelFactory): ViewModelProvider.Factory
}