package crazy.ones.unwiretest.di.module

import androidx.lifecycle.ViewModel
import crazy.ones.unwiretest.di.ViewModelKey
import crazy.ones.unwiretest.presentation.feature.shirtDetails.ShirtDetailsActivity
import crazy.ones.unwiretest.presentation.feature.shirtDetails.ShirtDetailsViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module
interface ShirtDetailsModule {

    @ContributesAndroidInjector
    fun shirtDetailsActivity(): ShirtDetailsActivity

    @Binds
    @IntoMap
    @ViewModelKey(ShirtDetailsViewModel::class)
    fun bindShirtDetailsViewModel(viewModel: ShirtDetailsViewModel): ViewModel
}