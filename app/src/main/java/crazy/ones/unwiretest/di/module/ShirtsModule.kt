package crazy.ones.unwiretest.di.module

import androidx.lifecycle.ViewModel
import crazy.ones.unwiretest.di.ViewModelKey
import crazy.ones.unwiretest.presentation.feature.shirts.ShirtsActivity
import crazy.ones.unwiretest.presentation.feature.shirts.ShirtsViewModel
import dagger.Binds
import dagger.Module
import dagger.android.ContributesAndroidInjector
import dagger.multibindings.IntoMap

@Module(includes = [ShirtDetailsModule::class, BasketModule::class])
interface ShirtsModule {

    @ContributesAndroidInjector
    fun shirtActivity(): ShirtsActivity

    @Binds
    @IntoMap
    @ViewModelKey(ShirtsViewModel::class)
    fun bindShirtsViewModel(viewModel: ShirtsViewModel): ViewModel
}