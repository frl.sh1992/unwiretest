package crazy.ones.unwiretest.di.component

import crazy.ones.unwiretest.di.module.AppModule
import crazy.ones.unwiretest.di.module.NetworkModule
import crazy.ones.unwiretest.di.module.ShirtsModule
import crazy.ones.unwiretest.di.module.ViewModelModule
import crazy.ones.unwiretest.presentation.MainApplication
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidSupportInjectionModule::class,
        NetworkModule::class,
        AppModule::class,
        ViewModelModule::class,
        ShirtsModule::class]
)
interface AppComponent : AndroidInjector<MainApplication> {

    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<MainApplication>()
}