package crazy.ones.unwiretest.business.entity

data class Shirt(
    val id: Long,
    val price: Long,
    val pictureUrl: String,
    val colour: String,
    val size: String,
    val name: String,
    val quantity: Int,
    val isInBasket: Boolean = false
)

data class FilterItems(val colors: MutableSet<String>, val sizes: MutableSet<String>)

data class Order(
    val id: String,
    val total: Long,
    val status: String,
    val shirts: List<Shirt>
)