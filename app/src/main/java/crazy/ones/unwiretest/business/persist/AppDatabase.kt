package crazy.ones.unwiretest.business.persist

import androidx.room.Database
import androidx.room.RoomDatabase

@Database(entities = [ShirtTable::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun getShirtDao(): ShirtDao
}