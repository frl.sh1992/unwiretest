package crazy.ones.unwiretest.business.network

interface ErrorParser {

    fun parse(throwable: Throwable): String
}