package crazy.ones.unwiretest.business.repository.shirt

import crazy.ones.unwiretest.business.entity.FilterItems
import crazy.ones.unwiretest.business.entity.Shirt
import io.reactivex.Flowable
import io.reactivex.Single

interface ShirtRepository {

    fun refresh(): Single<List<Shirt>>

    fun getAll(): Flowable<List<Shirt>>

    fun toggleShirtFromBasket(shirt: Shirt)

    fun getInBasketShirts(): Flowable<List<Shirt>>

    fun clearBasket()

    fun get(shirtId: Long): Flowable<Shirt>

    fun get(filterItems: FilterItems): Flowable<List<Shirt>>
}