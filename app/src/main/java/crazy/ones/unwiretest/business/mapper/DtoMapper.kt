package crazy.ones.unwiretest.business.mapper

import crazy.ones.unwiretest.business.entity.Order
import crazy.ones.unwiretest.business.entity.Shirt
import crazy.ones.unwiretest.business.network.OrderDto
import crazy.ones.unwiretest.business.network.ShirtDto

fun ShirtDto.toModel(): Shirt =
    Shirt(id, price, pictureUrl, colour, size, name, quantity)

fun OrderDto.toModel(): Order = Order(id, total, status, shirts.map { it.toModel() })