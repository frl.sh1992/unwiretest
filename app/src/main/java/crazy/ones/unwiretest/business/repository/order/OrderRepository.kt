package crazy.ones.unwiretest.business.repository.order

import crazy.ones.unwiretest.business.entity.Order
import crazy.ones.unwiretest.business.entity.Shirt
import io.reactivex.Single

interface OrderRepository {

    fun submitOrder(shirts: List<Shirt>): Single<Order>
}