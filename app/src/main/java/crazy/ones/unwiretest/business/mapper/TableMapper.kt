package crazy.ones.unwiretest.business.mapper

import crazy.ones.unwiretest.business.entity.Shirt
import crazy.ones.unwiretest.business.persist.ShirtTable

fun ShirtTable.toModel() = Shirt(id, price, pictureUrl, colour, size, name, quantity, isInBasket)