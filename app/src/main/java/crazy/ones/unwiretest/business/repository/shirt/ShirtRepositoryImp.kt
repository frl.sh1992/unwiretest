package crazy.ones.unwiretest.business.repository.shirt

import crazy.ones.unwiretest.business.entity.FilterItems
import crazy.ones.unwiretest.business.entity.Shirt
import crazy.ones.unwiretest.business.mapper.toModel
import crazy.ones.unwiretest.business.mapper.toTable
import crazy.ones.unwiretest.business.network.ShirtApi
import crazy.ones.unwiretest.business.persist.ShirtDao
import io.reactivex.Flowable
import io.reactivex.Single
import io.reactivex.functions.BiFunction

class ShirtRepositoryImp(private val shirtApi: ShirtApi, private val shirtDao: ShirtDao) :
    ShirtRepository {

    override fun getAll(): Flowable<List<Shirt>> =
        shirtDao.getAll().map { list ->
            list.map { it.toModel() }
        }

    override fun get(filterItems: FilterItems): Flowable<List<Shirt>> =
        shirtDao.getByFilter(filterItems.colors.toList(), filterItems.sizes.toList()).map { list ->
            list.map { it.toModel() }
        }

    override fun refresh(): Single<List<Shirt>> =
        Single.zip(
            shirtApi.getShirts().map { dto ->
                dto.map { it.toModel() }
            },
            shirtDao.getInBasketShirts().first(listOf()).map { list ->
                list.map { it.toModel() }
            },
            BiFunction<List<Shirt>, List<Shirt>, List<Shirt>> { shirts, inBasketShirts ->
                shirts.map { shirt ->
                    inBasketShirts.find { it.id == shirt.id } ?: shirt
                }
            }).map { list ->
            list.map {
                it.toTable()
            }.let {
                shirtDao.insert(it)
                list
            }
        }

    override fun getInBasketShirts(): Flowable<List<Shirt>> =
        shirtDao.getInBasketShirts().map { list ->
            list.map { it.toModel() }
        }

    override fun toggleShirtFromBasket(shirt: Shirt) =
        shirtDao.update(shirt.copy(isInBasket = !shirt.isInBasket).toTable())

    override fun clearBasket() =
        shirtDao.clearBasket()

    override fun get(shirtId: Long): Flowable<Shirt> =
        shirtDao.get(shirtId).map { it.toModel() }
}