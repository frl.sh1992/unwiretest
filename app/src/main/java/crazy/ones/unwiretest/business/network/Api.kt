package crazy.ones.unwiretest.business.network

import io.reactivex.Single
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST

interface ShirtApi {
    @GET("shirts")
    fun getShirts(): Single<List<ShirtDto>>
}

interface OrderApi {
    @POST("order")
    fun createOrder(@Body createOrderRequestDto: CreateOrderRequestDto): Single<OrderDto>
}