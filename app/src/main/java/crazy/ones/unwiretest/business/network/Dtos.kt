package crazy.ones.unwiretest.business.network

import com.google.gson.annotations.SerializedName

data class ShirtDto(
    @SerializedName("id") val id: Long,
    @SerializedName("price") val price: Long,
    @SerializedName("picture") val pictureUrl: String,
    @SerializedName("colour") val colour: String,
    @SerializedName("size") val size: String,
    @SerializedName("name") val name: String,
    @SerializedName("quantity") val quantity: Int
)

data class BasketDto(
    @SerializedName("shirts") val shirts: List<ShirtDto>
)

data class OrderDto(
    // TODO: id is string in api but it is integer in documentation...
    @SerializedName("id") val id: String,
    @SerializedName("total") val total: Long,
    @SerializedName("status") val status: String,
    @SerializedName("shirts") val shirts: List<ShirtDto>
)

data class CreateOrderRequestDto(
    @SerializedName("total") val total: Long,
    @SerializedName("basket") val basket: BasketDto
)