package crazy.ones.unwiretest.business.mapper

import crazy.ones.unwiretest.business.entity.Shirt
import crazy.ones.unwiretest.business.network.BasketDto
import crazy.ones.unwiretest.business.network.CreateOrderRequestDto
import crazy.ones.unwiretest.business.network.ShirtDto
import crazy.ones.unwiretest.business.persist.ShirtTable

fun List<Shirt>.toCreateOrderDto(): CreateOrderRequestDto =
    CreateOrderRequestDto(
        // TODO: why does this api work just with 0 for total price
        0 //this.sumBy { it.price.toInt() }.toLong()
        , this.toBasketDto())

fun List<Shirt>.toBasketDto(): BasketDto = BasketDto(this.map { it.toDto() })

fun Shirt.toDto(): ShirtDto = ShirtDto(id, price, pictureUrl, colour, size, name, quantity)

fun Shirt.toTable(): ShirtTable = ShirtTable(id, price, pictureUrl, colour, size, name, quantity, isInBasket)