package crazy.ones.unwiretest.business.repository.order

import crazy.ones.unwiretest.business.entity.Order
import crazy.ones.unwiretest.business.entity.Shirt
import crazy.ones.unwiretest.business.mapper.toCreateOrderDto
import crazy.ones.unwiretest.business.mapper.toModel
import crazy.ones.unwiretest.business.network.OrderApi
import io.reactivex.Single

class OrderRepositoryImp(private val orderApi: OrderApi) : OrderRepository {

    override fun submitOrder(shirts: List<Shirt>): Single<Order> =
        orderApi.createOrder(shirts.toCreateOrderDto()).map { it.toModel() }
}