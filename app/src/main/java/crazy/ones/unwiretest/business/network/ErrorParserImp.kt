package crazy.ones.unwiretest.business.network

import android.content.Context
import crazy.ones.unwiretest.R

class ErrorParserImp(private val context: Context) : ErrorParser {

    override fun parse(throwable: Throwable): String {
        return throwable.message?.let { it } ?: run {
            context.getString(R.string.error_message)
        }
    }
}