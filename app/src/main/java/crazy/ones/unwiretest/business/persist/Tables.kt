package crazy.ones.unwiretest.business.persist

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "shirt")
data class ShirtTable(
    @PrimaryKey
    @ColumnInfo(name = "id")
    val id: Long,
    @ColumnInfo(name = "price")
    val price: Long,
    @ColumnInfo(name = "picture_url")
    val pictureUrl: String,
    @ColumnInfo(name = "colour")
    val colour: String,
    @ColumnInfo(name = "size")
    val size: String,
    @ColumnInfo(name = "name")
    val name: String,
    @ColumnInfo(name = "quantity")
    val quantity: Int,
    @ColumnInfo(name = "is_in_basket")
    val isInBasket: Boolean = false
)