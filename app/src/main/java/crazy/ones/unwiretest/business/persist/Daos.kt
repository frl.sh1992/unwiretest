package crazy.ones.unwiretest.business.persist

import androidx.room.*
import io.reactivex.Flowable

@Dao
interface ShirtDao {

    @Query("SELECT * FROM shirt")
    fun getAll(): Flowable<List<ShirtTable>>

    @Query("SELECT * FROM shirt WHERE colour IN (:colors) OR size IN (:sizes)")
    fun getByFilter(colors: List<String>, sizes: List<String>): Flowable<List<ShirtTable>>

    @Query("SELECT * FROM shirt WHERE is_in_basket = :isAdded")
    fun getInBasketShirts(isAdded: Boolean = true): Flowable<List<ShirtTable>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(shirts: List<ShirtTable>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(shirt: ShirtTable)

    @Update
    fun update(shirt: ShirtTable)

    @Query("UPDATE shirt SET is_in_basket = :clear")
    fun clearBasket(clear: Boolean = false)

    @Query("SELECT * FROM shirt WHERE id = :shirtId")
    fun get(shirtId: Long): Flowable<ShirtTable>
}